﻿using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;
using MarketManAPI.Entity;
using MarketManAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MarketManAPI.Data
{
    public class CelebsContext
    {
        public List<Celeb> Celebs { get; set; }

        public CelebsContext()
        {
            Celebs = new List<Celeb>();
            Task.Run(async() => { 
                await Load();
                SaveChanges();
            });
        }

        public void SaveChanges()
        {
            string json = JsonConvert.SerializeObject(Celebs.ToArray(), Formatting.Indented);

            string path = Directory.GetCurrentDirectory() + @"\jsondata.txt";

            //write string to file
            System.IO.File.WriteAllText(path, json);
        }
        public async Task Load()
        {
            List<Celeb> tempList = new List<Celeb>();
            string siteHost = "https://www.imdb.com";
            string listUrl = "/list/ls052283250";

            IHtmlDocument document = await ScrapeWebsite(siteHost + listUrl);
            List<string> celebsPath = GetCelebsPathFromDoc(document);

            foreach (var path in celebsPath)
            {
                IHtmlDocument celebDoc = await ScrapeWebsite(siteHost + path);
                tempList.Add(GetCelebFromDoc(celebDoc));
               
                ///////// for debug only//////////
                //
                //if (tempList.Count == 10) break;
                //
                ////////////////////////////////
            }

            Celebs = tempList;

        }

        private async Task<IHtmlDocument> ScrapeWebsite(string siteUrl)
        {
            CancellationTokenSource cancellationToken = new CancellationTokenSource();
            HttpClient httpClient = new HttpClient();
            HttpResponseMessage request = await httpClient.GetAsync(siteUrl);
            cancellationToken.Token.ThrowIfCancellationRequested();

            Stream response = await request.Content.ReadAsStreamAsync();
            cancellationToken.Token.ThrowIfCancellationRequested();

            HtmlParser parser = new HtmlParser();
            return parser.ParseDocument(response);

        }
        private List<string> GetCelebsPathFromDoc(IHtmlDocument document)
        {
            string baseJson = document.QuerySelector("script[type=\"application/ld+json\"]").InnerHtml;
            var data = (JObject)JsonConvert.DeserializeObject(baseJson);
            JArray itemListElement = data["about"]["itemListElement"].Value<JArray>();

            List<string> vipList = new List<string>();
            foreach (var item in itemListElement)
            {
                foreach (JProperty itemChild in item.Children())
                {
                    if (itemChild.Name == "url")
                    {
                        string url = itemChild.Value.Value<string>();
                        vipList.Add(url);
                    }
                }
            }
            return vipList;

        }
        private Celeb GetCelebFromDoc(IHtmlDocument document)
        {
            string baseJson = document.QuerySelector("script[type=\"application/ld+json\"]").InnerHtml;
            var data = (JObject)JsonConvert.DeserializeObject(baseJson);

            Celeb newCeleb = new Celeb();
            newCeleb.ID = Guid.NewGuid();
            foreach (JProperty itemChild in data.Children())
            {
                if (itemChild.Name == "name")
                {
                    newCeleb.Name = itemChild.Value.Value<string>();
                }
                else if (itemChild.Name == "image")
                {
                    newCeleb.ImagePath = new Uri(itemChild.Value.Value<string>());
                }
                else if (itemChild.Name == "birthDate")
                {
                    newCeleb.BirthDate = DateTime.Parse(itemChild.Value.Value<string>());
                }
                else if (itemChild.Name == "jobTitle")
                {
                    JArray titles = itemChild.Value.Value<JArray>();
                    newCeleb.JobTitle = titles.Select(job => (string)job).ToArray();
                }

            }
            
            // the difference between male and female found on Actor/Actress JobTilte onle
            // can maybe improve it later by searching description to identify she/he in text
            newCeleb.Gender = Gender.Unspecified;
            if (newCeleb.JobTitle.Contains("Actress"))
            {
                newCeleb.Gender = Gender.Female;
            } else if (newCeleb.JobTitle.Contains("Actor"))
            {
                newCeleb.Gender = Gender.Male;
            }

            return newCeleb;

        }

         
    }
}
