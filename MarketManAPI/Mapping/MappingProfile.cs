﻿using AutoMapper;
using MarketManAPI.Entity;
using MarketManAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketManAPI.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Celeb, CelebDto>();
            CreateMap<CelebDto, Celeb>();
            CreateMap<CelebForCreationDto, Celeb>()
                .ForMember(
                    dest => dest.ID,
                    opt => opt.MapFrom(src => Guid.NewGuid()));

            CreateMap<CelebForUpdateDto, Celeb>();
        }
    }
}
