﻿using MarketManAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketManAPI.Entity
{
    public class Celeb
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public string[] JobTitle { get; set; }
        public Uri ImagePath { get; set; }

    }
}
