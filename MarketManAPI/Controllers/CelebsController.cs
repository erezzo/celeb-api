﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MarketManAPI.Entity;
using MarketManAPI.Models;
using MarketManAPI.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MarketManAPI.Controllers
{
    [ApiController]
    [Route("api/celebs")]
    public class CelebsController : ControllerBase
    {
        private readonly ICelebsRepository _celebsRepository;
        private readonly IMapper _mapper;

        public CelebsController(ICelebsRepository celebsRepository, IMapper mapper)
        {
            _celebsRepository = celebsRepository;
            _mapper = mapper;
        }
                
        [HttpGet]
        [HttpHead]
        [EnableCors("AllowOrigin")]
        public ActionResult<IEnumerable<CelebDto>> GetCelebs()
        {
            var celebs = _celebsRepository.GetCelebs();
            return Ok(_mapper.Map<IEnumerable<CelebDto>>(celebs));
        }

        [HttpGet("{guid}", Name = "GetCeleb")]
        public ActionResult<IEnumerable<CelebDto>> GetCeleb(Guid guid)
        {
            if (guid == Guid.Empty)
                return BadRequest();

            var celeb = _celebsRepository.GetCeleb(guid);
            if (celeb == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<CelebDto>(celeb));
        }

        [HttpPost]
        public ActionResult<CelebDto> CreateCeleb([FromBody] CelebForCreationDto celeb)
        {
            var celebEntity = _mapper.Map<Celeb>(celeb);
            _celebsRepository.AddCeleb(celebEntity);
            _celebsRepository.Save();

            var celebToReturn = _mapper.Map<CelebDto>(celebEntity);
            return CreatedAtRoute("GetCeleb",
                new { guid = celebToReturn.ID },
                celebToReturn);
        }
        [HttpPost("reset", Name = "Reset")]
        [EnableCors("AllowOrigin")]
        public async Task<ActionResult<CelebDto>> Reset()
        {
           await _celebsRepository.Reset();
           
            var celebs = _celebsRepository.GetCelebs();
            return Ok(_mapper.Map<IEnumerable<CelebDto>>(celebs));            
        }

        [HttpPut("{guid}")]
        public ActionResult<CelebDto> UpdateCeleb(Guid guid, [FromBody] CelebForCreationDto celebUpdate)
        {
            var celebToUpdate = _celebsRepository.GetCeleb(guid);

            if (celebToUpdate == null)
                return NotFound();

            var celebUpdateSource = _mapper.Map<Celeb>(celebUpdate);

            // for PUT we update all Celeb fields
            _celebsRepository.UpdateCeleb(celebToUpdate, celebUpdateSource, true);
            _celebsRepository.Save();

            return Ok(_mapper.Map<CelebDto>(celebToUpdate));
        }

        [HttpPatch("{guid}")]
        public ActionResult<CelebDto> UpdateCeleb(Guid guid, [FromBody] CelebForUpdateDto celebUpdate)
        {
            var celebToUpdate = _celebsRepository.GetCeleb(guid);

            if (celebToUpdate == null)
                return NotFound();

            var celebUpdateSource = _mapper.Map<Celeb>(celebUpdate);

            // for PATCH we update only JobTile and ImagePath fields
            _celebsRepository.UpdateCeleb(celebToUpdate, celebUpdateSource, false);
            _celebsRepository.Save();

            return Ok(_mapper.Map<CelebDto>(celebToUpdate));
        }

        [HttpDelete("{guid}")]
        [EnableCors("AllowOrigin")]
        public IActionResult DeleteCeleb(Guid guid)
        {
            if (guid == Guid.Empty)
                return BadRequest();

            var celeb = _celebsRepository.GetCeleb(guid);

            _celebsRepository.DeleteCeleb(celeb);
            _celebsRepository.Save();

            return NoContent();
        }

    }
}
