﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketManAPI.Models
{
    public enum Gender { Unspecified = 0, Male = 1, Female = 2 }
   
    public class CelebDto
    {
        public Guid ID { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public Gender Gender { get; set; }
        public string[] JobTitle { get; set; }
        public Uri ImagePath { get; set; }
    }
}
