﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketManAPI.Models
{
    public class CelebForUpdateDto
    {
        public string[] JobTitle { get; set; }
        public Uri ImagePath { get; set; }
    }
}
