﻿using MarketManAPI.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MarketManAPI.Services
{
    public interface ICelebsRepository
    {
        IEnumerable<Celeb> GetCelebs();
        Celeb GetCeleb(Guid celebId);
        void AddCeleb(Celeb celeb);
        void DeleteCeleb(Celeb celeb);
        void UpdateCeleb(Celeb celebToUpdate, Celeb celebInfo, bool fullUpdate);
        void Save();
        Task Reset();
    }
}
