﻿using AngleSharp.Html.Dom;
using AngleSharp.Html.Parser;
using MarketManAPI.Data;
using MarketManAPI.Entity;
using MarketManAPI.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace MarketManAPI.Services
{
    public class CelebsRepository: ICelebsRepository
    {
        
        private readonly CelebsContext _context;

        public CelebsRepository(CelebsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public IEnumerable<Celeb> GetCelebs()
        {
            return _context.Celebs;
        }

        public Celeb GetCeleb(Guid celebId)
        {
            return _context.Celebs.FirstOrDefault(a => a.ID == celebId);
        }

        public void AddCeleb(Celeb celeb)
        {
            if (celeb == null)
            {
                throw new ArgumentNullException(nameof(celeb));
            }

            celeb.ID = Guid.NewGuid();

            _context.Celebs.Add(celeb);
        }

        public void UpdateCeleb(Celeb celebToUpdate, Celeb celebInfo, bool fullUpdate)
        {            
            celebToUpdate.JobTitle = celebInfo.JobTitle;
            celebToUpdate.ImagePath = celebInfo.ImagePath;

            if (fullUpdate)
            {
                celebToUpdate.Name = celebInfo.Name;
                celebToUpdate.BirthDate = celebInfo.BirthDate;
                celebToUpdate.Gender = celebInfo.Gender;
            }
        }

        public void DeleteCeleb(Celeb celeb)
        {
            _context.Celebs.Remove(celeb);
        }
        public async Task Reset()
        {
            await _context.Load();
            _context.SaveChanges();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

    }
}
